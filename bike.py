import csv, datetime, math

startLocIndex = 4
startTimeIndex = 2
endLocIndex = 7
endTimeIndex = 5

def toDate(s):
    return datetime.datetime.strptime(s, '%m/%d/%y %H:%M').date()

def getFirstRebalance(reader):
    locationCount = dict()
    bikesInTransit = list()
    next(reader)

    for row in reader:
        startLocation = row[startLocIndex]
        locationCount[startLocation] = locationCount.get(startLocation, 30) - 1
        bikesInTransit.append(row)

        # Update all bikes that finished ride before current bike
        for bike in bikesInTransit:
            if toDate(bike[endTimeIndex]) <= toDate(row[startTimeIndex]):
                locationCount[bike[endLocIndex]] = locationCount.get(bike[endLocIndex], 30) + 1
                bikesInTransit.remove(bike)

        if locationCount[startLocation] < 0:
            return row[startTimeIndex]

    return "No need to rebalance"

def getMostPopularDate(reader):
    output = dict()
    next(reader)

    for row in reader:
        key = toDate(row[2]).weekday()
        output[key] = output.get(key, 0) + 1

    return max(output.keys(), key=(lambda key: output[key]))

def getAvgLen(reader):
    output = dict()
    next(reader)
    bikeNoIndex = 8
    for row in reader:
        output[row[bikeNoIndex]] = output.get(row[bikeNoIndex], 0) + int(row[1])

    return sum(output.values()) / len(output)

def main():
    reader = csv.reader(open("bikeData.csv"))
    a = getAvgLen(reader)
    reader = csv.reader(open("bikeData.csv"))
    b = getMostPopularDate(reader)
    reader = csv.reader(open("bikeData.csv"))
    c = getFirstRebalance(reader)
    print(math.floor(a))
    print(b)
    print(c)

main()
